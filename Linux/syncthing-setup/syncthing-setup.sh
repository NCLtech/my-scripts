#!/bin/bash
# Elevate Privelage

echo "This Script has to be ran as Sudo"
sudo -v

sudo apt install net-tools
# Set Variables
ip=$(sudo ifconfig eth0 | perl -ne 'print $1 if /inet\s.*?(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\b/')

echo "Please enter Syncthing Version required"

read syncthingversion

# Setup Folders

sudo mkdir /opt/syncthing
sudo chmod 777 /opt/syncthing
mkdir /opt/syncthing/config
mkdir /opt/syncthing/bin
mkdir /opt/syncthing/bin/current

# Get Syncthing and setup
cd /opt/syncthing/bin
wget https://github.com/syncthing/syncthing/releases/download/v$syncthingversion/syncthing-linux-amd64-v$syncthingversion.tar.gz
untar syncthing-linux-amd64-v$syncthingversion.tar.gz
cp -r syncthing-linux-amd64-v$syncthingversion/* /opt/syncthing/bin/current
sudo ln -f /opt/syncthing/bin/current/syncthing /usr/local/bin/syncthing

# Setup Supervisor

sudo apt-get install supervisor

sudo cat <<EOF > /etc/supervisor/conf.d/syncthing.conf
[program:syncthing]
autorestart = True
directory = /opt/syncthing/config
user = phil
command = /usr/local/bin/syncthing -no-browser -logflags=0 -gui-address=$ip:8384 -home=/opt/syncthing/config/
environment = STNORESTART="1", HOME="/opt/syncthing/config/"
EOF

sudo service supervisor restart
sudo supervisorctl reread
sudo supervisorctl update

echo "If everything installed with no errors"
echo "Please go to http://$ip:8384"
